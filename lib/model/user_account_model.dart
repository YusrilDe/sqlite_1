class UserAccountModel {
  String imageUrl;
  String userName;
  String email;

  UserAccountModel({this.imageUrl, this.userName, this.email});

  UserAccountModel.fromJson(Map<String, dynamic> json) {
    imageUrl = json['imageUrl'];
    userName = json['userName'];
    email = json['email'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, String> data = Map<String, String>();
    data['imageUrl'] = this.imageUrl;
    data['userName'] = this.userName;
    data['email'] = this.email;
    return data;
  }
}
