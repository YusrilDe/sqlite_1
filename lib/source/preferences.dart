import 'package:shared_preferences/shared_preferences.dart';

class Preferences {
  static String keyUserAccount = "key_user";
  static String keyFavorite = "key_favorite";

  static void setAccountUser(String userAccountData) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(keyUserAccount, userAccountData);
  }

  static Future<String> getUserAccount() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(keyUserAccount);
  }

  static void setFavorite(String favoriteCharacter) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(keyFavorite, favoriteCharacter);
  }

  static Future<String> getFavorite() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    return prefs.getString(keyFavorite);
  }
}
