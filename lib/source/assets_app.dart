class AssetsApp {
  static String splashscreenImg = "assets/splashscreen_img.jpg";
  static String googleLogoImg = "assets/google_logo_img.png";

  static String maleImg = "assets/male.png";
  static String femaleImg = "assets/female.png";
  static String nonGenderImg = "assets/non_gender.png";
  static String loginImg = "assets/sw_logo.png";
}
