import 'package:flutter/material.dart';
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/views/main/detail/detail_screen.dart';

class SearchPage extends StatefulWidget {
  @override
  _SearchPageState createState() => _SearchPageState();
}

class _SearchPageState extends State<SearchPage> {
  List<Result> listSearchChara = List<Result>();
  List<Result> passVariableWithoutAwait = List<Result>();

  TextEditingController searchController = TextEditingController();
  var resultDB;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBarApp(),
      backgroundColor: Color(
        AppColors.primaryColorMainApp,
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            searchBar(),
            listSearch(),
          ],
        ),
      ),
    );
  }

  Widget appBarApp() {
    return AppBar(
      backgroundColor: Color(AppColors.primaryColorMainApp),
      elevation: 0,
      title: Text(
        "Search Page",
        style: TextStyle(
          color: Colors.white,
          fontFamily: "OpenSans500",
          fontSize: 18,
        ),
      ),
    );
  }

  Widget formSearch() {
    return Expanded(
      child: Container(
        margin: EdgeInsets.symmetric(
          horizontal: 15,
          vertical: 15,
        ),
        height: 50,
        child: TextFormField(
          controller: searchController,
          decoration: InputDecoration(
            labelText: "Search Characters",
            hintText: "Luke Sky",
            hintStyle: TextStyle(
              fontSize: 14,
              color: Colors.grey,
              fontFamily: "OpenSans400",
            ),
            labelStyle: TextStyle(
              fontFamily: "OpenSans400",
              fontSize: 16,
              color: Colors.white.withOpacity(0.6),
            ),
            focusedBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(5),
              ),
              borderSide: BorderSide(
                color: Colors.white.withOpacity(0.6),
                width: 1,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderRadius: BorderRadius.all(
                Radius.circular(15),
              ),
              borderSide: BorderSide(
                color: Colors.white.withOpacity(0.6),
                width: 1,
              ),
            ),
          ),
          style: TextStyle(
            color: Colors.white.withOpacity(0.6),
          ),
        ),
      ),
    );
  }

  Widget submitSearch() {
    return InkWell(
      onTap: () async {
        if (searchController.text.isEmpty || searchController.text == "") {
          listSearchChara = await DatabaseProvider.db.getAllCharacters();
        }
        listSearchChara =
            await DatabaseProvider.db.searchCharacters(searchController.text);
        setState(() {
          passVariableWithoutAwait = listSearchChara;
        });
      },
      child: Container(
        margin: EdgeInsets.only(
          right: 15,
        ),
        height: 50,
        width: 50,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
          border: Border.all(
            color: Colors.white,
            width: 1,
          ),
        ),
        child: Icon(
          Icons.arrow_forward,
          color: Colors.white,
        ),
      ),
    );
  }

  Widget searchBar() {
    return Row(
      children: [
        formSearch(),
        submitSearch(),
      ],
    );
  }

  Widget listSearch() {
    return Container(
      height: MediaQuery.of(context).size.height,
      child: ListView.builder(
        physics: ClampingScrollPhysics(),
        shrinkWrap: true,
        itemCount: listSearchChara.length,
        itemBuilder: (BuildContext context, int index) => Container(
          margin: EdgeInsets.only(right: 0),
          child: InkWell(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => DetailScreen(
                    characterStarWars: listSearchChara[index],
                  ),
                ),
              );
            },
            child: listItems(index),
          ),
        ),
      ),
    );
  }

  Widget listItems(int index) {
    return Card(
      margin: EdgeInsets.only(
        left: 10,
        right: 10,
        bottom: 10,
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 20,
        ),
        child: Text(
          listSearchChara[index].name,
          style: TextStyle(
            fontSize: 16,
            fontFamily: "OpenSans500",
            color: Color(AppColors.primaryColorMainApp),
          ),
        ),
      ),
    );
  }
}
