import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/source/preferences.dart';
import 'package:sqlite_1/views/main/detail/detail_screen.dart';

class FavoritesPage extends StatefulWidget {
  @override
  _FavoritesPageState createState() => _FavoritesPageState();
}

class _FavoritesPageState extends State<FavoritesPage> {
  List<Result> _favorite = List<Result>();

  @override
  void initState() {
    Preferences.getFavorite().then((value) {
      print("Checking value of wishlist 1 : $value");
      if (value != null && value is String) {
        var result = jsonDecode(value) as List;
        setState(() {
          result.forEach((element) {
            _favorite.add(
              Result.fromJson(element),
            );
            _favorite.reversed.toList();
          });
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Color(
          AppColors.primaryColorMainApp,
        ),
        title: Text(
          "Favorites Characters",
          style: TextStyle(
            color: Colors.white,
            fontFamily: "OpenSans500",
          ),
        ),
      ),
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: Container(
        margin: EdgeInsets.only(
          top: 10,
        ),
        height: MediaQuery.of(context).size.height,
        child: ListView.builder(
          physics: ClampingScrollPhysics(),
          shrinkWrap: true,
          itemCount: _favorite.length,
          itemBuilder: (BuildContext context, int index) => Container(
            child: InkWell(
              onTap: () {
                Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => DetailScreen(
                      characterStarWars: _favorite[index],
                    ),
                  ),
                );
              },
              child: favoriteItems(index),
            ),
          ),
        ),
      ),
    );
  }

  Widget favoriteItems(int index) {
    return Card(
      margin: EdgeInsets.only(
        left: 10,
        right: 10,
        bottom: 10,
      ),
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              _favorite[index].name,
              style: TextStyle(
                fontSize: 16,
                fontFamily: "OpenSans500",
                color: Color(AppColors.primaryColorMainApp),
              ),
            ),
            IconButton(
                icon: Icon(
                  Icons.close,
                  size: 18,
                ),
                onPressed: () {
                  _favorite.removeWhere(
                      (element) => element.name == _favorite[index].name);
                  Preferences.setFavorite(jsonEncode(_favorite));
                  setState(() {});
                }),
          ],
        ),
      ),
    );
  }
}
