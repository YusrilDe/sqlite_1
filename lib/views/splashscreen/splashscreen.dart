import 'package:flutter/material.dart';
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/service/api.dart';
import 'package:sqlite_1/source/assets_app.dart';
import 'package:sqlite_1/source/preferences.dart';
import 'package:sqlite_1/views/auth/login.dart';
import 'package:sqlite_1/views/main/main_screen.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    Preferences.getUserAccount().then((value) {
      if (value == null) {
        _loadFromAPI();
      } else {
        _loadFromSQLite();
      }
    });
    super.initState();
  }

  _loadFromAPI() async {
    GetData.api.request("people", onSuccess: (dataFromAPI) {
      if (dataFromAPI != null) {
        Future.delayed(Duration(milliseconds: 1500), () {
          Future.delayed(Duration(seconds: 3), () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => LoginPage(),
              ),
            );
          });
        });
      }
    }, sendToDBLocale: true);
  }

  _loadFromSQLite() async {
    var resultDB = await DatabaseProvider.db.getAllCharacters();
    Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => MainScreen(
          listCharacters: resultDB,
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Image.asset(
        AssetsApp.splashscreenImg,
        fit: BoxFit.cover,
      ),
    );
  }
}
