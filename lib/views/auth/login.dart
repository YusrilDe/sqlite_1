import 'package:flutter/material.dart';
import 'package:sqlite_1/service/auth.dart';
import 'package:sqlite_1/source/assets_app.dart';
import 'package:sqlite_1/source/colors.dart';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  TextEditingController emailController = TextEditingController(text: "");
  TextEditingController passwordController = TextEditingController(text: "");

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
          ),
          appDescription(),
          appImage(),
          googleSignInMethodApp(),
        ],
      ),
    );
  }

  Widget googleSignInMethodApp() {
    return Positioned.fill(
      child: Align(
        alignment: Alignment.bottomCenter,
        child: Container(
          height: 50,
          margin: EdgeInsets.symmetric(
            horizontal: 36,
            vertical: 20,
          ),
          child: InkWell(
            onTap: () {
              Authentication.auth.onGoogleSignIn(context);
            },
            child: Container(
              width: 180,
              height: 50,
              padding: EdgeInsets.all(0),
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(25),
                ),
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset(
                    AssetsApp.googleLogoImg,
                    width: 35,
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Text(
                    "Login with Google",
                    style: TextStyle(
                      fontFamily: "OpenSans500",
                      color: Color(AppColors.primaryColorMainApp),
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget appImage() {
    return Positioned.fill(
      child: Align(
        alignment: Alignment.center,
        child: Container(
          margin: EdgeInsets.all(16),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height / 2.5,
          child: Image.asset(
            AssetsApp.loginImg,
            color: Colors.white,
          ),
        ),
      ),
    );
  }

  Widget appDescription() {
    return Positioned.fill(
      top: 100,
      child: Align(
        alignment: Alignment.topCenter,
        child: Container(
          padding: EdgeInsets.all(0),
          child: Text(
            "Welcome to Star Wars Character Encyclopedia",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 25,
              fontFamily: "OpenSans800",
              color: Colors.white,
            ),
          ),
        ),
      ),
    );
  }
}
