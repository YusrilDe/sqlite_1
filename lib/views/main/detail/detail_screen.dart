import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/source/assets_app.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/source/preferences.dart';
import 'package:sqlite_1/views/main/queries/update.dart';

class DetailScreen extends StatefulWidget {
  Result characterStarWars;
  DetailScreen({this.characterStarWars});
  @override
  _DetailScreenState createState() => _DetailScreenState();
}

class _DetailScreenState extends State<DetailScreen> {
  @override
  void initState() {
    Preferences.getFavorite().then((value) {
      if (value != null && value is String) {
        var result = jsonDecode(value) as List;
        setState(() {
          result.forEach((element) {
            _favorite.add(
              Result.fromJson(element),
            );
          });
          _favorite.forEach((element) {
            if (element.name == widget.characterStarWars.name) {
              isBecomeFavorite = true;
            }
          });
        });
      }
    });
    super.initState();
  }

  List<Result> _favorite = List<Result>();

  bool isBecomeFavorite = false;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(AppColors.primaryColorMainApp),
        elevation: 0,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(widget.characterStarWars.name),
            InkWell(
              onTap: () {
                setState(
                  () {
                    if (isBecomeFavorite) {
                      isBecomeFavorite = false;
                      _favorite.removeWhere((element) =>
                          element.name == widget.characterStarWars.name);

                      Preferences.setFavorite(jsonEncode(_favorite));
                    } else {
                      Fluttertoast.showToast(
                          msg: "Berhasil ditambahkan ke favorite",
                          toastLength: Toast.LENGTH_SHORT,
                          gravity: ToastGravity.CENTER,
                          timeInSecForIosWeb: 1,
                          backgroundColor: Colors.green,
                          textColor: Colors.white,
                          fontSize: 16.0);
                      isBecomeFavorite = true;

                      //sharedPref
                      var check = _favorite.where((element) =>
                          element.name == widget.characterStarWars.name);
                      if (check.isEmpty) {
                        _favorite.add(
                          Result(
                            name: widget.characterStarWars.name,
                            height: widget.characterStarWars.height,
                            mass: widget.characterStarWars.mass,
                            hairColor: widget.characterStarWars.hairColor,
                            skinColor: widget.characterStarWars.skinColor,
                            eyeColor: widget.characterStarWars.eyeColor,
                            birthYear: widget.characterStarWars.birthYear,
                            gender: widget.characterStarWars.gender,
                          ),
                        );

                        Preferences.setFavorite(
                          jsonEncode(_favorite),
                        );
                      }
                    }
                  },
                );
              },
              child: Container(
                margin: EdgeInsets.only(top: 0, right: 16),
                child: Icon(
                  !isBecomeFavorite ? Icons.favorite_border : Icons.favorite,
                  color: !isBecomeFavorite ? Colors.grey : Colors.red,
                ),
              ),
            ),
          ],
        ),
      ),
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(
            vertical: 10,
            horizontal: 15,
          ),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    child: characterDataType(),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  characterDataResult(),
                ],
              ),
              SizedBox(
                height: 20,
              ),
              InkWell(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => EditCharacters(
                        widget.characterStarWars,
                      ),
                    ),
                  );
                },
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 40,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(
                      Radius.circular(5),
                    ),
                  ),
                  alignment: Alignment.center,
                  child: Text(
                    "Edit Profile Character",
                    style: TextStyle(
                        color: Color(
                          AppColors.primaryColorMainApp,
                        ),
                        fontSize: 16,
                        fontFamily: "OpenSans500"),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Widget characterDataResult() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
          children: [
            characterResult(
              widget.characterStarWars.name,
              withColon: true,
            ),
            SizedBox(
              width: 10,
            ),
            Image.asset(
              widget.characterStarWars.gender.toString() == "Gender.MALE"
                  ? AssetsApp.maleImg
                  : widget.characterStarWars.gender.toString() ==
                          "Gender.FEMALE"
                      ? AssetsApp.femaleImg
                      : AssetsApp.nonGenderImg,
              width: 15,
              height: 15,
              color: Colors.white,
            ),
          ],
        ),
        characterResult(
          widget.characterStarWars.height,
          withColon: true,
        ),
        characterResult(
          widget.characterStarWars.mass,
          withColon: true,
        ),
        characterResult(
          widget.characterStarWars.hairColor,
          withColon: true,
        ),
        characterResult(
          widget.characterStarWars.skinColor,
          withColon: true,
        ),
        characterResult(
          widget.characterStarWars.eyeColor,
          withColon: true,
        ),
        characterResult(
          widget.characterStarWars.birthYear,
          withColon: true,
        ),
      ],
    );
  }

  Widget characterDataType() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        characterResult(
          "Name",
        ),
        characterResult(
          "Height",
        ),
        characterResult(
          "Mass",
        ),
        characterResult(
          "Hair Color",
        ),
        characterResult(
          "Color Skin",
        ),
        characterResult(
          "Eye Color",
        ),
        characterResult(
          "Birth Year",
        ),
      ],
    );
  }

  Widget characterResult(String results, {bool withColon = false}) {
    return Text(
      "${withColon ? ":" : ""}     ${results.toString()}",
      style: TextStyle(
        color: Colors.white,
        fontFamily: "OpenSans500",
        fontSize: 16,
      ),
    );
  }
}
