import 'dart:convert';
import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/model/user_account_model.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/source/preferences.dart';
import 'package:sqlite_1/views/favorites/favorites.dart';
import 'package:sqlite_1/views/main/list/grid.dart';
import 'package:sqlite_1/views/main/list/vertical.dart';
import 'package:sqlite_1/views/main/queries/create_characters.dart';
import 'package:sqlite_1/views/profile/profile.dart';
import 'package:sqlite_1/views/search/search_page.dart';

class MainScreen extends StatefulWidget {
  GoogleSignIn googleSignIn;
  User user;
  List<Result> listCharacters;
  MainScreen({this.listCharacters, this.googleSignIn, this.user});

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen>
    with SingleTickerProviderStateMixin {
  TabController _tabController;
  List<UserAccountModel> userAccountList = List<UserAccountModel>();
  Map<String, dynamic> dataUserAccount = Map<String, dynamic>();

  @override
  void initState() {
    setState(() {});
    _tabController = new TabController(length: 2, vsync: this);
    Preferences.getUserAccount().then((value) {
      if (value != null && value is String) {
        var result = jsonDecode(value);
        setState(() {
          dataUserAccount = result;
        });
      }
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    var mediaWidth = MediaQuery.of(context).size.width;

    return Scaffold(
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: WillPopScope(
          child: Column(
            children: [
              SizedBox(
                height: kToolbarHeight,
              ),
              searchFavoritesAndProfile(mediaWidth),
              greetings(),
              tabBarItems(),
              tabBarView(),
            ],
          ),
          onWillPop: () {
            return exit(0);
          }),
      floatingActionButton: FloatingActionButton.extended(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => CreateCharactersPage(),
            ),
          );
        },
        backgroundColor: Colors.green,
        icon: Icon(Icons.add),
        label: Text(
          "Create",
          style: TextStyle(color: Colors.white),
        ),
      ),
    );
  }

  Widget searchFavoritesAndProfile(var mediaWidth) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        initSearch(mediaWidth),
        initFavorites(),
        initProfile(),
      ],
    );
  }

  Widget initProfile() {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => ProfilePage(dataUserAccount),
          ),
        );
      },
      child: Container(
        width: 40,
        height: 40,
        decoration: BoxDecoration(
          color: Colors.white,
          shape: BoxShape.circle,
          image: DecorationImage(
            image: NetworkImage(
              dataUserAccount["imageUrl"] == null
                  ? "https://pertaniansehat.com/v01/wp-content/uploads/2015/08/default-placeholder.png"
                  : dataUserAccount["imageUrl"],
            ),
          ),
        ),
        alignment: Alignment.center,
      ),
    );
  }

  Widget initFavorites() {
    return IconButton(
      onPressed: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => FavoritesPage(),
          ),
        );
      },
      icon: Icon(
        Icons.favorite_border,
        color: Colors.white,
      ),
    );
  }

  Widget initSearch(var mediaWidth) {
    return InkWell(
      onTap: () {
        Navigator.push(
          context,
          MaterialPageRoute(
            builder: (context) => SearchPage(),
          ),
        );
      },
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
        ),
        width: mediaWidth / 1.5,
        height: 50,
        decoration: BoxDecoration(
          color: Color(AppColors.primaryColorMainApp),
          border: Border.all(
            color: Colors.white,
            width: 1,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        alignment: Alignment.centerLeft,
        child: Text(
          "Search Character",
          style: TextStyle(
            fontSize: 16,
            color: Colors.white.withOpacity(0.7),
            fontFamily: "OpenSans400",
          ),
        ),
      ),
    );
  }

  Widget greetings() {
    return Container(
      padding: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 16,
      ),
      child: Text(
        "Hello ${dataUserAccount["userName"]}",
        textAlign: TextAlign.left,
        style: TextStyle(color: Colors.white, fontFamily: "OpenSans500"),
      ),
    );
  }

  Widget tabBarItems() {
    return TabBar(
      unselectedLabelColor: Colors.black,
      labelColor: Colors.red,
      indicatorColor: Colors.white,
      tabs: [
        Tab(
          icon: Icon(
            Icons.list,
            color: Colors.white,
          ),
        ),
        Tab(
          icon: Icon(
            Icons.grid_on,
            color: Colors.white,
          ),
        )
      ],
      controller: _tabController,
      indicatorSize: TabBarIndicatorSize.tab,
    );
  }

  Widget tabBarView() {
    return Expanded(
      child: TabBarView(
        controller: _tabController,
        children: [
          VerticalList(
            listCharacters: widget.listCharacters,
          ),
          GridList(
            listCharacters: widget.listCharacters,
          )
        ],
      ),
    );
  }
}
