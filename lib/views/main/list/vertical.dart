import 'package:flutter/material.dart';
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/views/main/detail/detail_screen.dart';
import 'package:sqlite_1/views/main/main_screen.dart';

class VerticalList extends StatefulWidget {
  List<Result> listCharacters;
  VerticalList({this.listCharacters});

  @override
  _VerticalListState createState() => _VerticalListState();
}

class _VerticalListState extends State<VerticalList> {
  @override
  void initState() {
    super.initState();
  }

  int tooglSort = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: Center(
        child: Column(
          children: [
            sort(),
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: ListView.builder(
                  physics: ClampingScrollPhysics(),
                  shrinkWrap: true,
                  reverse: tooglSort == 1 ? true : false,
                  itemCount: widget.listCharacters.length,
                  itemBuilder: (BuildContext context, int index) => Container(
                    child: InkWell(
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DetailScreen(
                              characterStarWars: widget.listCharacters[index],
                            ),
                          ),
                        );
                      },
                      child: listItems(index),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 70,
            ),
          ],
        ),
      ),
    );
  }

  Widget listItems(int index) {
    return Card(
      child: Container(
        padding: EdgeInsets.symmetric(
          horizontal: 10,
          vertical: 10,
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              widget.listCharacters[index].name,
              style: TextStyle(
                fontSize: 16,
                fontFamily: "OpenSans500",
                color: Color(AppColors.primaryColorMainApp),
              ),
            ),
            buttonDelete(index),
          ],
        ),
      ),
    );
  }

  Widget buttonDelete(int index) {
    return IconButton(
      onPressed: () {
        showDialog<String>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            title: Text('Delete Character'),
            content: Text(
                'Are you sure to delete ${widget.listCharacters[index].name} ?'),
            actions: <Widget>[
              InkWell(
                onTap: () {
                  Navigator.pop(context, 'Cancel');
                },
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 10),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(15),
                    ),
                    border: Border.all(
                      color: Colors.black,
                      width: 1,
                    ),
                  ),
                  child: Text('Cancel'),
                ),
              ),
              InkWell(
                onTap: () async {
                  await DatabaseProvider.db
                      .deleteCharacter(widget.listCharacters[index].name);
                  var resultDB = await DatabaseProvider.db.getAllCharacters();
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => MainScreen(
                        listCharacters: resultDB,
                      ),
                    ),
                  );
                },
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 0),
                  padding: EdgeInsets.all(10),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(
                      Radius.circular(15),
                    ),
                    border: Border.all(
                      color: Colors.black,
                      width: 1,
                    ),
                  ),
                  child: Text('Yes'),
                ),
              ),
            ],
          ),
        );
      },
      icon: Icon(
        Icons.delete,
        color: Color(
          AppColors.primaryColorMainApp,
        ),
      ),
    );
  }

  Widget sort() {
    return InkWell(
      onTap: () {
        setState(() {
          if (tooglSort == 0) {
            tooglSort = 1;
          } else {
            tooglSort = 0;
          }
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: 10, top: 10),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(),
        child: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          child: Icon(Icons.sort_by_alpha),
        ),
      ),
    );
  }
}
