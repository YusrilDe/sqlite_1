import 'package:flutter/material.dart';
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/views/main/detail/detail_screen.dart';
import 'package:sqlite_1/views/main/main_screen.dart';

class GridList extends StatefulWidget {
  List<Result> listCharacters;
  GridList({this.listCharacters});

  @override
  _GridListState createState() => _GridListState();
}

class _GridListState extends State<GridList> {
  int tooglSort = 0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: Center(
        child: Column(
          children: [
            sort(),
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: GridView.builder(
                  reverse: tooglSort == 1 ? true : false,
                  itemCount: widget.listCharacters.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2),
                  itemBuilder: (context, index) {
                    return Container(
                      child: InkWell(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => DetailScreen(
                                characterStarWars: widget.listCharacters[index],
                              ),
                            ),
                          );
                        },
                        child: Stack(
                          children: [
                            listItemName(index),
                            buttonDelete(index),
                          ],
                        ),
                      ),
                    );
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget listItemName(int index) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 10,
      ),
      color: Colors.white,
      width: 160,
      height: 160,
      alignment: Alignment.center,
      child: Text(
        widget.listCharacters[index].name,
        textAlign: TextAlign.center,
        style: TextStyle(
          fontSize: 16,
          fontFamily: "OpenSans500",
          color: Color(AppColors.primaryColorMainApp),
        ),
      ),
    );
  }

  Widget buttonDelete(int index) {
    return Positioned(
      right: 15,
      top: 15,
      child: InkWell(
        onTap: () {
          showDialog<String>(
            context: context,
            builder: (BuildContext context) => AlertDialog(
              title: Text('Delete Character'),
              content: Text(
                  'Are you sure to delete ${widget.listCharacters[index].name} ?'),
              actions: <Widget>[
                InkWell(
                  onTap: () {
                    Navigator.pop(context, 'Cancel');
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      border: Border.all(
                        color: Colors.black,
                        width: 1,
                      ),
                    ),
                    child: Text('Cancel'),
                  ),
                ),
                InkWell(
                  onTap: () async {
                    await DatabaseProvider.db
                        .deleteCharacter(widget.listCharacters[index].name);
                    var resultDB = await DatabaseProvider.db.getAllCharacters();
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => MainScreen(
                          listCharacters: resultDB,
                        ),
                      ),
                    );
                  },
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 0),
                    padding: EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                        Radius.circular(15),
                      ),
                      border: Border.all(
                        color: Colors.black,
                        width: 1,
                      ),
                    ),
                    child: Text('Yes'),
                  ),
                ),
              ],
            ),
          );
        },
        child: Icon(
          Icons.delete,
          size: 18,
        ),
      ),
    );
  }

  Widget sort() {
    return InkWell(
      onTap: () {
        setState(() {
          if (tooglSort == 0) {
            tooglSort = 1;
          } else {
            tooglSort = 0;
          }
        });
      },
      child: Container(
        margin: EdgeInsets.only(left: 10, top: 10),
        alignment: Alignment.centerLeft,
        decoration: BoxDecoration(),
        child: Container(
          width: 50,
          height: 50,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.all(
              Radius.circular(10),
            ),
          ),
          child: Icon(Icons.sort_by_alpha),
        ),
      ),
    );
  }
}
