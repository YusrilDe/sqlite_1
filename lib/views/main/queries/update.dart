import 'package:flutter/material.dart';
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/views/main/main_screen.dart';

class EditCharacters extends StatefulWidget {
  Result characters;
  EditCharacters(this.characters);
  @override
  _EditCharactersState createState() => _EditCharactersState();
}

class _EditCharactersState extends State<EditCharacters> {
  TextEditingController charaNameController = TextEditingController();
  TextEditingController charaHeightController = TextEditingController();
  TextEditingController charaMassController = TextEditingController();
  TextEditingController charaHairColorController = TextEditingController();
  TextEditingController charaColorSkinController = TextEditingController();
  TextEditingController charaEyeColorController = TextEditingController();
  TextEditingController charaBirthYearController = TextEditingController();
  TextEditingController charaGenderController = TextEditingController();

  @override
  void initState() {
    charaNameController.text = widget.characters.name;
    charaHeightController.text = widget.characters.height;
    charaMassController.text = widget.characters.mass;
    charaHairColorController.text = widget.characters.hairColor;
    charaColorSkinController.text = widget.characters.skinColor;
    charaEyeColorController.text = widget.characters.eyeColor;
    charaBirthYearController.text = widget.characters.birthYear;
    charaGenderController.text =
        widget.characters.gender.toString() == "Gender.MALE"
            ? "Male"
            : widget.characters.gender.toString() == "Gender.FEMALE"
                ? "Female"
                : "n/a";
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: appBar(),
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: SingleChildScrollView(
        child: Column(
          children: [
            columnFormInput(),
            submitEditData(),
          ],
        ),
      ),
    );
  }

  Widget columnFormInput() {
    return Column(
      children: [
        inputForm(
            label: "Character Name",
            hint: "Luke",
            controller: charaNameController),
        inputForm(
            label: "Character Height",
            hint: "185",
            controller: charaHeightController),
        inputForm(
            label: "Character Mass",
            hint: "85",
            controller: charaMassController),
        inputForm(
            label: "Character Hair Color",
            hint: "Pink",
            controller: charaHairColorController),
        inputForm(
            label: "Character Color Skin",
            hint: "Yellow",
            controller: charaColorSkinController),
        inputForm(
            label: "Character Eye Color",
            hint: "blue",
            controller: charaEyeColorController),
        inputForm(
            label: "Character BirthYear",
            hint: "34BBY",
            controller: charaBirthYearController),
        inputForm(
          label: "Character Gender",
          hint: "Male",
          controller: charaGenderController,
        ),
      ],
    );
  }

  Widget submitEditData() {
    return InkWell(
      onTap: () async {
        showDialog<String>(
          context: context,
          builder: (BuildContext context) => AlertDialog(
            title: Text('Update Character'),
            content:
                Text('Are you sure to update data ${widget.characters.name} ?'),
            actions: <Widget>[
              cancelAction(),
              submitAction(),
            ],
          ),
        );
      },
      child: editButton(),
    );
  }

  Widget cancelAction() {
    return InkWell(
      onTap: () {
        Navigator.pop(context, 'Cancel');
      },
      child: cancelButton(),
    );
  }

  Widget submitAction() {
    return InkWell(
      onTap: () async {
        DatabaseProvider.db.updateDataCharacters(
          charaNameController.text,
          double.parse(charaHeightController.text),
          double.parse(charaMassController.text),
          charaHairColorController.text,
          charaColorSkinController.text,
          charaEyeColorController.text,
          charaBirthYearController.text,
          charaGenderController.text.toLowerCase(),
        );
        var resultDB = await DatabaseProvider.db.getAllCharacters();
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => MainScreen(
              listCharacters: resultDB,
            ),
          ),
        );
      },
      child: yesButton(),
    );
  }

  Widget cancelButton() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 10),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(
          color: Colors.black,
          width: 1,
        ),
      ),
      child: Text('Cancel'),
    );
  }

  Widget yesButton() {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 0),
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.all(
          Radius.circular(15),
        ),
        border: Border.all(
          color: Colors.black,
          width: 1,
        ),
      ),
      child: Text('Yes'),
    );
  }

  Widget editButton() {
    return Container(
      height: 50,
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 16,
        vertical: 20,
      ),
      decoration: BoxDecoration(
        color: Colors.white,
        border: Border.all(
          width: 1,
          color: Colors.white,
        ),
        borderRadius: BorderRadius.all(
          Radius.circular(25),
        ),
      ),
      alignment: Alignment.center,
      child: Text(
        "Edit",
        style: TextStyle(
          color: Color(AppColors.primaryColorMainApp),
          fontFamily: "OpenSans500",
          fontSize: 16,
        ),
      ),
    );
  }

  Widget appBar() {
    return AppBar(
      backgroundColor: Color(AppColors.primaryColorMainApp),
      elevation: 0,
      title: Text(
        "Edit Characters Data",
        style: TextStyle(
          color: Colors.white,
          fontSize: 16,
          fontFamily: "OpenSans500",
        ),
      ),
    );
  }

  Widget inputForm(
      {String label, String hint, TextEditingController controller}) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 15,
      ),
      height: 50,
      child: TextFormField(
        decoration: InputDecoration(
          labelText: label,
          hintText: hint,
          hintStyle: TextStyle(
            fontSize: 14,
            color: Colors.grey,
            fontFamily: "OpenSans400",
          ),
          labelStyle: TextStyle(
            fontFamily: "OpenSans400",
            fontSize: 16,
            color: Colors.white.withOpacity(0.6),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(5),
            ),
            borderSide: BorderSide(
              color: Colors.white.withOpacity(0.6),
              width: 1,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              color: Colors.white.withOpacity(0.6),
              width: 1,
            ),
          ),
        ),
        style: TextStyle(
          color: Colors.white.withOpacity(0.6),
        ),
        controller: controller,
      ),
    );
  }
}
