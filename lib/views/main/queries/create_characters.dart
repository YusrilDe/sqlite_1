import 'package:flutter/material.dart';
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/views/main/main_screen.dart';

class CreateCharactersPage extends StatefulWidget {
  @override
  _CreateCharactersPageState createState() => _CreateCharactersPageState();
}

class _CreateCharactersPageState extends State<CreateCharactersPage> {
  TextEditingController charaNameController = TextEditingController();
  TextEditingController charaHeightController = TextEditingController();
  TextEditingController charaMassController = TextEditingController();
  TextEditingController charaHairColorController = TextEditingController();
  TextEditingController charaColorSkinController = TextEditingController();
  TextEditingController charaEyeColorController = TextEditingController();
  TextEditingController charaBirthYearController = TextEditingController();
  TextEditingController charaGenderController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(AppColors.primaryColorMainApp),
        title: Text("Make new Character"),
        elevation: 0,
      ),
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: SingleChildScrollView(
        child: Column(
          children: [
            informUser(),
            columnFormInput(),
          ],
        ),
      ),
    );
  }

  Widget submitButton() {
    return InkWell(
      onTap: () async {
        DatabaseProvider.db.insertNewCharacter(
          charaNameController.text,
          double.parse(charaHeightController.text),
          double.parse(charaMassController.text),
          charaHairColorController.text,
          charaColorSkinController.text,
          charaEyeColorController.text,
          charaBirthYearController.text,
          charaGenderController.text,
        );
        var resultDB = await DatabaseProvider.db.getAllCharacters();
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => MainScreen(
              listCharacters: resultDB,
            ),
          ),
        );
      },
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        margin: EdgeInsets.symmetric(
          horizontal: 16,
          vertical: 20,
        ),
        decoration: BoxDecoration(
          color: Colors.white,
          border: Border.all(
            width: 1,
            color: Colors.white,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(25),
          ),
        ),
        alignment: Alignment.center,
        child: Text(
          "Create",
          style: TextStyle(
            color: Color(AppColors.primaryColorMainApp),
            fontFamily: "OpenSans500",
            fontSize: 16,
          ),
        ),
      ),
    );
  }

  Widget columnFormInput() {
    return Column(
      children: [
        inputForm(
            label: "Character Name",
            hint: "Luke",
            controller: charaNameController),
        inputForm(
            label: "Character Height",
            hint: "185",
            controller: charaHeightController),
        inputForm(
            label: "Character Mass",
            hint: "85",
            controller: charaMassController),
        inputForm(
            label: "Character Hair Color",
            hint: "Pink",
            controller: charaHairColorController),
        inputForm(
            label: "Character Color Skin",
            hint: "Yellow",
            controller: charaColorSkinController),
        inputForm(
            label: "Character Eye Color",
            hint: "blue",
            controller: charaEyeColorController),
        inputForm(
            label: "Character BirthYear",
            hint: "34BBY",
            controller: charaBirthYearController),
        inputForm(
          label: "Character Gender",
          hint: "Male",
          controller: charaGenderController,
        ),
      ],
    );
  }

  Widget informUser() {
    return Container(
      width: MediaQuery.of(context).size.width,
      margin: EdgeInsets.symmetric(
        horizontal: 15,
      ),
      child: Text(
        "Create new character here",
        textAlign: TextAlign.start,
        style: TextStyle(
          color: Colors.white,
          fontFamily: "OpenSans400",
          fontSize: 14,
        ),
      ),
    );
  }

  Widget inputForm(
      {String label, String hint, TextEditingController controller}) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: 15,
        vertical: 15,
      ),
      height: 50,
      child: TextFormField(
        decoration: InputDecoration(
          labelText: label,
          hintText: hint,
          hintStyle: TextStyle(
            fontSize: 14,
            color: Colors.grey,
            fontFamily: "OpenSans400",
          ),
          labelStyle: TextStyle(
            fontFamily: "OpenSans400",
            fontSize: 16,
            color: Colors.white.withOpacity(0.6),
          ),
          focusedBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(5),
            ),
            borderSide: BorderSide(
              color: Colors.white.withOpacity(0.6),
              width: 1,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.all(
              Radius.circular(15),
            ),
            borderSide: BorderSide(
              color: Colors.white.withOpacity(0.6),
              width: 1,
            ),
          ),
        ),
        style: TextStyle(
          color: Colors.white.withOpacity(0.6),
        ),
        controller: controller,
      ),
    );
  }
}
