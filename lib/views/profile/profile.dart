import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:sqlite_1/model/user_account_model.dart';
import 'package:sqlite_1/source/colors.dart';
import 'package:sqlite_1/source/preferences.dart';
import 'package:sqlite_1/views/auth/login.dart';
import 'package:sqlite_1/views/favorites/favorites.dart';
import 'package:sqlite_1/views/main/queries/create_characters.dart';
import 'package:sqlite_1/views/search/search_page.dart';

class ProfilePage extends StatefulWidget {
  Map<String, dynamic> userAccount = Map<String, dynamic>();
  ProfilePage(this.userAccount);
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Color(AppColors.primaryColorMainApp),
        elevation: 0,
        title: Text(
          "Profile",
          style: TextStyle(
            color: Colors.white,
            fontFamily: "OpenSans500",
            fontSize: 18,
          ),
        ),
      ),
      backgroundColor: Color(AppColors.primaryColorMainApp),
      body: SingleChildScrollView(
        child: Container(
          margin: EdgeInsets.symmetric(
            horizontal: 16,
            vertical: 20,
          ),
          child: Column(
            children: [
              userData(),
              height20(),
              lineSeparator(),
              height20(),
              menu(),
              height20(),
              submitButton(),
            ],
          ),
        ),
      ),
    );
  }

  Widget submitButton() {
    return InkWell(
      onTap: () {
        Preferences.setAccountUser(null);
        Navigator.pushReplacement(
          context,
          MaterialPageRoute(
            builder: (context) => LoginPage(),
          ),
        );
      },
      child: submitButtonBuilder(),
    );
  }

  Widget submitButtonBuilder() {
    return Container(
      width: 100,
      height: 45,
      decoration: BoxDecoration(
        color: Colors.red,
        borderRadius: BorderRadius.all(
          Radius.circular(10),
        ),
        border: Border.all(
          color: Colors.white,
          width: 1,
        ),
      ),
      alignment: Alignment.center,
      child: Text(
        "Log out",
        style: TextStyle(
          color: Colors.white,
          fontFamily: "OpenSans500",
          fontSize: 16,
        ),
      ),
    );
  }

  Widget menu() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        menuOnProfile(
          menuName: "Favorites",
          iconMenu: Icon(
            Icons.favorite,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => FavoritesPage(),
              ),
            );
          },
        ),
        menuOnProfile(
          menuName: "Search",
          iconMenu: Icon(
            Icons.search,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SearchPage(),
              ),
            );
          },
        ),
        menuOnProfile(
          menuName: "Create",
          iconMenu: Icon(
            Icons.add_box,
            color: Colors.white,
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CreateCharactersPage(),
              ),
            );
          },
        ),
      ],
    );
  }

  Widget userData() {
    return Row(
      children: [
        profilePicture(),
        width20(),
        nameAndEmail(),
      ],
    );
  }

  Widget nameAndEmail() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        userName(),
        email(),
      ],
    );
  }

  Widget userName() {
    return Text(
      widget.userAccount["userName"],
      style: TextStyle(
        color: Colors.white,
        fontFamily: "OpenSans",
      ),
    );
  }

  Widget email() {
    return Container(
      child: Text(
        widget.userAccount["email"],
        maxLines: 2,
        style: TextStyle(
          fontSize:
              widget.userAccount["email"].toString().length > 25 ? 12 : 14,
          color: Colors.white,
          fontFamily: "OpenSans",
        ),
      ),
    );
  }

  Widget profilePicture() {
    return Container(
      width: 80,
      height: 80,
      decoration: BoxDecoration(
        image: DecorationImage(
          image: NetworkImage(widget.userAccount["imageUrl"]),
        ),
        shape: BoxShape.circle,
        border: Border.all(
          color: Colors.white,
          width: 1.5,
        ),
      ),
    );
  }

  Widget height20() {
    return SizedBox(
      height: 20,
    );
  }

  Widget width20() {
    return SizedBox(
      width: 20,
    );
  }

  Widget lineSeparator() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 1,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
    );
  }

  Widget menuOnProfile({String menuName, Icon iconMenu, Function onTap}) {
    return InkWell(
      onTap: onTap,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.white,
            width: 1,
          ),
          borderRadius: BorderRadius.all(
            Radius.circular(15),
          ),
        ),
        width: 70,
        height: 70,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            iconMenu,
            Text(
              menuName,
              style: TextStyle(color: Colors.white, fontFamily: "OpenSans"),
            ),
          ],
        ),
      ),
    );
  }
}
