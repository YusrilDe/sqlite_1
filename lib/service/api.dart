import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/model/model.dart';
import 'package:sqlite_1/service/config.dart';

class GetData {
  static final GetData api = GetData._();
  GetData._();

  List data;

  request(
    String request, {
    Function(dynamic) onSuccess,
    Function(Error error) onError,
    bool sendToDBLocale = false,
  }) async {
    var res = await http.get(Uri.encodeFull("${Config.baseUrl}/$request"),
        headers: {"Accept": "application/json"});
    print("99 checking data from net called func and the res : $res");
    var resBody = json.decode(res.body);
    data = resBody["results"];
    if (onSuccess != null) {
      if (sendToDBLocale) {
        onSuccess(data.map((e) {
          DatabaseProvider.db.initCharacters(Result.fromJson(e));
        }).toList());
      }
      onSuccess(data);
    }
  }

  Future<List<Result>> initDataToDBLocale() async {
    var res = await http.get(Uri.encodeFull(Config.baseUrl),
        headers: {"Accept": "application/json"});

    var resBody = json.decode(res.body);
    data = resBody["results"];

    return data.map((e) {
      print("xxx Checkin data map $e");
      DatabaseProvider.db.initCharacters(Result.fromJson(e));
    }).toList();
  }
}
