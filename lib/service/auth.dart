import 'dart:convert';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:sqlite_1/service/locale/db_provider.dart';
import 'package:sqlite_1/model/user_account_model.dart';
import 'package:sqlite_1/source/preferences.dart';
import 'package:sqlite_1/views/main/main_screen.dart';

class Authentication {
  static final Authentication auth = Authentication._();
  Authentication._();

  GoogleSignIn _googleSignIn = GoogleSignIn();
  FirebaseAuth _auth;

  List<UserAccountModel> userAccountList = List<UserAccountModel>();

  void initApp() async {
    FirebaseApp defaultApp = await Firebase.initializeApp();
    _auth = FirebaseAuth.instanceFor(app: defaultApp);
  }

  void onGoogleSignIn(BuildContext context, {bool isSignIn = false}) async {
    initApp();
    User user = await _handleSignIn(isSignIn);
    saveUser(user);

    var resultDB = await DatabaseProvider.db.getAllCharacters();
    await Navigator.pushReplacement(
      context,
      MaterialPageRoute(
        builder: (context) => MainScreen(
          listCharacters: resultDB,
          user: user,
          googleSignIn: _googleSignIn,
        ),
      ),
    );
  }

  saveUser(User userAccount) {
    var checkingSave = userAccountList
        .where((element) => element.userName == userAccount.displayName);
    if (checkingSave.isEmpty) {
      Preferences.setAccountUser(
        jsonEncode(
          UserAccountModel(
            imageUrl: userAccount.photoURL,
            userName: userAccount.displayName,
            email: userAccount.email,
          ),
        ),
      );
    }
  }

  Future<User> _handleSignIn(bool isSignIn) async {
    User user;
    print("Chekcing boolean $isSignIn");

    if (isSignIn) {
      user = _auth.currentUser;
    } else {
      final GoogleSignInAccount googleUser = await _googleSignIn.signIn();
      final GoogleSignInAuthentication googleAuth =
          await googleUser.authentication;

      final AuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );

      user = (await _auth.signInWithCredential(credential)).user;
    }
    return user;
  }
}
