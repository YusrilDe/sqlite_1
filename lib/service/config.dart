class Config{

  static String _path = '/api/';
  static String _mainDomain = 'https://swapi.dev';
  static String get baseUrl => _mainDomain + _path;
}