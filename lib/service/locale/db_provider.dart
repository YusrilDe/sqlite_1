import 'dart:io';

import 'package:sqlite_1/model/model.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';
import 'dart:async';

class DatabaseProvider {
  static Database _database;
  static final DatabaseProvider db = DatabaseProvider._();

  DatabaseProvider._();

  Future<Database> get database async {
    if (_database != null) {
      return _database;
    }

    _database = await initDatabase();
    return _database;
  }

  initDatabase() async {
    Directory documentDirectory = await getApplicationDocumentsDirectory();
    final path = join(documentDirectory.path, 'star_wars_people.db');

    return await openDatabase(path, version: 1, onOpen: (db) {},
        onCreate: (Database db, int version) async {
      await db.execute(
          'CREATE TABLE StarWarsCharacters (id INTEGER PRIMARY KEY, name TEXT, height TEXT, mass TEXT, hair_color TEXT, skin_color TEXT, eye_color TEXT, birth_year TEXT, gender TEXT)');
    });
  }

  initCharacters(Result charResult) async {
    await deleteAllCharacters();
    final db = await database;
    final initResult =
        await db.insert('StarWarsCharacters', charResult.toJson());
    return initResult;
  }

  Future<int> deleteAllCharacters() async {
    final db = await database;
    final deletedResult = await db.rawDelete('Delete From StarWarsCharacters');

    return deletedResult;
  }

  Future<List<Result>> getAllCharacters() async {
    final db = await database;
    final result = await db.rawQuery("SELECT * FROM StarWarsCharacters");

    List<Result> list =
        result.isNotEmpty ? result.map((c) => Result.fromJson(c)).toList() : [];
    return list;
  }

  searchCharacters(String keyword)async{
    final db = await database;
    final resultSearch = await db.rawQuery("Select * from StarWarsCharacters where name like '%$keyword%'");

    List<Result> list =
        resultSearch.isNotEmpty ? resultSearch.map((c) => Result.fromJson(c)).toList() : [];
    return list;
  }

  updateDataCharacters(
    String name,
    double height,
    double mass,
    String hairColor,
    String skinColor,
    String eyeColor,
    String birthYear,
    String gender,
  ) async {
    final db = await database;
    final resultUpdate = await db.rawQuery(
        "Update StarWarsCharacters set name = '$name', height = '$height', mass = '$mass', hair_color = '$hairColor', skin_color = '$skinColor', eye_color = '$eyeColor', birth_year = '$birthYear', gender = '$gender' where name = '$name'");
    return resultUpdate;
  }

  insertNewCharacter(
    String name,
    double height,
    double mass,
    String hairColor,
    String skinColor,
    String eyeColor,
    String birthYear,
    String gender,
  ) async {
    print("insertNewCharacter function called");
    final db = await database;
    final resultInsert = await db.rawQuery(
        'Insert into StarWarsCharacters (name, height, mass, hair_color, skin_color, eye_color, birth_year, gender) values (?, ?, ?, ?, ?, ?, ?, ?)',
        [
          name,
          height,
          mass,
          hairColor,
          skinColor,
          eyeColor,
          birthYear,
          gender
        ]);
    return resultInsert;
  }

  deleteCharacter(String name) async {
    final db = await database;
    final resultDeleteCharacter = await db
        .rawQuery("Delete from StarWarsCharacters where name = '$name'");
    return resultDeleteCharacter;
  }
}
